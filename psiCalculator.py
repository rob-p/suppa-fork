# -*- coding: utf-8 -*-
"""
Created on Tue May  6 12:19:45 2014

@author: Gael P Alamancos
@email: gael.perez[at]upf.edu
"""

import sys
import logging
from argparse import ArgumentParser, RawTextHelpFormatter
from lib.tools import *
from lib.event import *

description = \
    "Description:\n\n" + \
    "This tool reads an ioe file and a transcript expression file and calculates\n" + \
    "the Percentage of Sliced In (PSI)\n"
    
parser = ArgumentParser(description = description, formatter_class=RawTextHelpFormatter,
                        add_help=False)
parser.add_argument("-i", "--ioe-file", help = "input ioe file.", required = True)
parser.add_argument("-e", "--expression-file", required = True,
                    help = "input expression file.")
parser.add_argument("-o", "--output-file", required = True,
                    help = "output psi file.")
parser.add_argument("-f", "--total-filter", type = float, default = 0,
                    help = "minimum accumulative expression of all the transcripts involve in the event.")
parser.add_argument("-m", "--mode", default="INFO",
                    help = "to choose from DEBUG, INFO, WARNING, ERROR and CRITICAL")

def main(): 
    args = parser.parse_args()
    try:
        #Parsing arguments
        mode = "logging." + args.mode
        
        #Setting logging preferences
        logger = logging.getLogger(__name__)
        logger.setLevel(eval(mode))
        
        #Setting the level of the loggers in lib
        setToolsLoggerLevel(mode)
        
        outputFile = args.output_file
        totalFilter = args.total_filter
        expressionFile = args.expression_file
    
        expressionDictionary = {} #to store transcript expression [transcript_id] = {[colId] = expression}
        psiDictionary = {} #to store the psi values calculated [event_id] = {[colId] = expression}
        colIds = [] #to store all the column_id for the expression fields 
            
        #Buffering EXPRESSION FILE
        factory = FactoryReader()
        r = factory.getReader("expression")
        r.openFile(expressionFile)
        logger.info("Buffering transcript expression levels.")
        line = r.readLine()
        try:
            while True:
                try:
                    arguments = line.next()
                    colIds = arguments["colIds"]
                    expressionDictionary[arguments["transcript_id"]] = {}
                    #Assign per each colId the expression [colId] = expression
                    for key, value in arguments["expression"].iteritems():
                        expressionDictionary[arguments["transcript_id"]][key] = \
                        float(value)
                except ValueError:
                    logger.error("%s expression is not a float. Skipping..." % arguments["transcript_id"])
                    continue
        except StopIteration:
            if not expressionDictionary: 
                logger.error("No expression values have been buffered.")
                sys.exit(1)
                
            #Buffering IOE and calculating PSI
            factory = FactoryReader()
            r = factory.getReader("ioe")
            r.openFile(args.ioe_file)
            logger.info("Calculating PSI from the ioe file.") 
            line = r.readLine()
            try:
                while True:
                    alternativeTranscripts = {}
                    totalTranscripts = {}
                    arguments = line.next()
                    if psiDictionary.get(arguments["event_id"]):
                        logger.error("Duplicated event %s. Skipping line..." %
                            arguments["event_id"])
                        continue
                    skip = False    #to avoid checking total_iso if event == "NA"
                    psiDictionary[arguments["event_id"]] = {}
                    for x in colIds:
                        #set to 0 all the cumulative expression values
                        alternativeTranscripts[x] = 0  
                        totalTranscripts[x] = 0
                    for tr in arguments["alt_iso"].rstrip("\n").split(","):
                        try:
                            for x in colIds:
                                alternativeTranscripts[x] += \
                                    expressionDictionary[tr][x] 
                        except KeyError:
                            logger.error(
                                "transcript %s not found in the \"expression file\"." %
                                tr)
                            logger.error("PSI not calculated for event %s." %
                                arguments["event_id"])                           
                            for x in colIds:
                                psiDictionary[arguments["event_id"]][x] = "NA"
                            skip = True
                    if not skip:
                        for tr in arguments["total_iso"].rstrip("\n").split(","):
                            try:
                                for x in colIds:
                                    totalTranscripts[x] += \
                                        expressionDictionary[tr][x]
                            except KeyError:
                                logger.error(
                                    "transcript %s not found in the \"expression file\"." %
                                    tr)
                                logger.error("PSI not calculated for event %s." %
                                    arguments["event_id"])
                                for x in colIds:
                                    psiDictionary[arguments["event_id"]][x] = "NA"
                                skip = True
                    for x in colIds:
                        #If it passes the filter and skip == False
                        if (totalTranscripts[x] >= totalFilter and not skip):
                            try:
                                psiDictionary[arguments["event_id"]][x] = (
                                    alternativeTranscripts[x]/totalTranscripts[x])
                            except ZeroDivisionError:
                                logger.debug("Zero division for event %s.(psi= -1)." % arguments["event_id"])
                                psiDictionary[arguments["event_id"]][x] = -1
                        #If it passes the filter but skip == True
                        elif not skip:   
                            #for x in colIds:
                                psiDictionary[arguments["event_id"]][x] = -1
            except StopIteration:
                writer = Writer.getWriter("PSI")
                logger.info("Generating output %s" % (outputFile + ".psi"))
                writer.openFile(outputFile)
                writer.writeLine("\t".join(colIds), False) 
                for key, value in sorted(psiDictionary.iteritems()):
                    logger.debug("Calculating psi for %s" %key)
                    psiLine = PsiWriter.lineGenerator(key, value, colIds)
                    writer.writeLine("\t".join(psiLine)) 
                writer.closeFile()
    except BaseException, err:
        logger.error("Unknown Error: %s" % err)
        sys.exit(1)
    logger.info("Done")

if __name__ == '__main__':
    main()